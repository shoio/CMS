<table class="table table-striped table-hover">
<h1>Menus</h1>
<a href="<?php echo BASE.'painel/menus_add'; ?>">Adicionar Menu</a><br>
	<thead>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>URL</th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
	    <?php foreach ($menus as $menu) : ?>
		<tr> 
			<td><?php echo $menu['id']; ?></td>
			<td><?php echo $menu['nome']; ?></td>
			<td><?php echo $menu['url']; ?></td>
			<td><a class="btn btn-info" href="<?php echo BASE; ?>painel/menus_edit/<?php echo $menu['id']; ?>">Editar</a> <a class="btn btn-warning" href="<?php echo BASE; ?>painel/menus_del/<?php echo $menu['id']; ?>">Excluir</a></td>
		</tr>
	    <?php endforeach; ?>
	</tbody>
</table>