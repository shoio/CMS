<a href="<?php echo BASE; ?>painel/pagina_add">Adicionar Página</a><br>
<table class="table table-striped table-hover">
	<thead>
	<h1>Páginas</h1>
		<tr>
			<th>ID</th>
			<th>URL</th>
			<th>Título</th>
			<th>Ações</th>
		</tr>
	</thead>
	<tbody>
	    <?php foreach ($paginas as $pagina) : ?>
		<tr> 
			<td><?php echo $pagina['id']; ?></td>
			<td><?php echo $pagina['url']; ?></td>
			<td><?php echo $pagina['titulo']; ?></td>
			<td><a href="<?php echo BASE; ?>painel/pagina_edit/<?php echo $pagina['id']; ?>"><button type="button" class="btn btn-info">Editar</button></a> <a href="<?php echo BASE; ?>painel/pagina_del/<?php echo $pagina['id']; ?>"><button type="button" class="btn btn-warning">Excluir</button></a></td>
		</tr>
	    <?php endforeach; ?>
	</tbody>
</table>