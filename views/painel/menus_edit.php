<form method="POST" role="form">
	<legend>Editar Menu</legend>

	<div class="form-group">
		<label for="">Nome do Menu</label>
		<input type="text" class="form-control" name="nome" value="<?php echo $menu['nome']; ?>">
	</div>

	<div class="form-group">
		<label for="">URL do Menu</label>
		<input type="text" class="form-control" name="url" value="<?php echo $menu['url']; ?>">
	</div>
	
	<button type="submit" class="btn btn-primary">Salvar</button>
</form>