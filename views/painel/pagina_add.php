<form method="POST" role="form">
	<legend>Adicionar Página</legend>

	<div class="form-group">
		<label for="">Título da Página</label>
		<input type="text" class="form-control" name="titulo">
	</div>

	<div class="form-group">
		<label for="">URL da Página</label>
		<input type="text" class="form-control" name="url">
	</div>

	<div class="form-group">
		<label for="">Criar menu automaticamente: </label>
		<input type="checkbox" class="checkbox-inline" name="add_menu" value="sim">
	</div>

	<div class="form-group">
		<label for="">Texto da Página</label>
		<textarea class="form-control" id="corpo" name="corpo"></textarea>
	</div>
	
	<button type="submit" class="btn btn-primary">Salvar</button>
</form>

<script type="text/javascript" src="<?php echo BASE; ?>ckeditor/ckeditor.js"></script>

<script type="text/javascript">
	window.onload = function(){
		CKEDITOR.replace("corpo");
	}
</script>