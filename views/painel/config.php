<form method="POST" role="form" enctype="multipart/form-data">
	<legend>Configurações</legend>

	<div class="form-group">
		<label for="">Título do Site</label>
		<input type="text" class="form-control" name="site_title" value="<?php echo $this->config['site_title']; ?>">
	</div>

	<div class="form-group">
		<label for="">Texto de boas vindas</label>
		<input type="text" class="form-control" name="home_welcome" value="<?php echo $this->config['home_welcome']; ?>">
	</div>
	
	<div>
		<label for="">Selecione a Template</label>
			<select name="site_template" class="form-control" required="required">
				<option value="default" <?php if ($this->config['site_template'] == "default") {
					echo 'selected = selected';
				} ?>>Padrão</option>
				<option value="promo" <?php if ($this->config['site_template'] == "promo") {
					echo 'selected = selected';
				} ?>>Promocional</option>
				<option value="natal" <?php if ($this->config['site_template'] == "natal") {
					echo 'selected = selected';
				} ?>>Natal</option>
			</select>
	</div><br>
	

	<button type="submit" class="btn btn-primary">Salvar</button>
</form>