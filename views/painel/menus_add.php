<form method="POST" role="form">
	<legend>Adicionar Menu</legend>

	<div class="form-group">
		<label for="">Nome do Menu</label>
		<input type="text" class="form-control" name="nome">
	</div>

	<div class="form-group">
		<label for="">URL do Menu</label>
		<input type="text" class="form-control" name="url">
	</div>
	
	<button type="submit" class="btn btn-primary">Salvar</button>
</form>