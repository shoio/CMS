<form method="POST" role="form">
	<legend>Editar Página</legend>

	<div class="form-group">
		<label for="">Título da Página</label>
		<input type="text" class="form-control" name="titulo" value="<?php echo $pagina['titulo']; ?>">
	</div>

	<div class="form-group">
		<label for="">URL da Página</label>
		<input type="text" class="form-control" name="url" value="<?php echo $pagina['url']; ?>">
	</div>

	<div class="form-group">
		<label for="">Texto da Página</label>
		<textarea class="form-control" id="corpo" name="corpo"><?php echo $pagina['corpo']; ?></textarea>
	</div>
	
	<button type="submit" class="btn btn-primary">Salvar</button>
</form>


<script type="text/javascript" src="<?php echo BASE; ?>ckeditor/ckeditor.js"></script>

<script type="text/javascript">
	window.onload = function(){
		CKEDITOR.replace("corpo");
	}
</script>